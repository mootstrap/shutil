#!/bin/sh
# -*- sh -*-
# Include the default shell utilities.
#
# Copyright (c) 2000-2019 Chris Sloan

# Run a tput command and get the return code.
test_tput() {
    tput "$@" >/dev/null 2>&1
    return "$?"
}

# If colors are not supported by the terminal, then just short circuit the rest
# of the file.  We have to make sure to shutil_provide the module or else it
# will be treated as an error.
if not test_tput colors; then
    shutil_provide colors
    return
fi

# Find out how many colors are supported.
max_colors=`tput colors`
[ "$?" -eq 0 ] || return

# Get the capabilities for setting foreground and background colors and the
# numbers associated with each color.
if test_tput setaf 0; then
    # We support ANSI sequences, so use those since they are portably defined.
    cap_f=setaf
    cap_b=setab
    black=0
    red=1
    green=2
    yellow=3
    blue=4
    magenta=5
    cyan=6
    white=7
else
    # Use the non-ANSI sequences.  These are the historical sequences and use a
    # different mapping for the colors.
    cap_f=setf
    cap_b=setb
    black=0
    blue=1
    green=2
    cyan=3
    red=4
    magenta=5
    yellow=6
    white=7
fi

# Define the foreground color escape sequences.
black_fg=`tput "$cap_f" "$black"`
red_fg=`tput "$cap_f" "$red"`
green_fg=`tput "$cap_f" "$green"`
yellow_fg=`tput "$cap_f" "$yellow"`
blue_fg=`tput "$cap_f" "$blue"`
magenta_fg=`tput "$cap_f" "$magenta"`
cyan_fg=`tput "$cap_f" "$cyan"`
white_fg=`tput "$cap_f" "$white"`

# Define the background color escape sequences.
black_bg=`tput "$cap_b" "$black"`
red_bg=`tput "$cap_b" "$red"`
green_bg=`tput "$cap_b" "$green"`
yellow_bg=`tput "$cap_b" "$yellow"`
blue_bg=`tput "$cap_b" "$blue"`
magenta_bg=`tput "$cap_b" "$magenta"`
cyan_bg=`tput "$cap_b" "$cyan"`
white_bg=`tput "$cap_b" "$white"`

if test_tput op; then
    # Restore the colors to the "original pair."  That is, to the defaults for
    # the terminal.
    orig_colors=`tput op`
else
    # If op is not supported, then assume the defaults are white on black.
    orig_colors="$black_bg$white_fg"
fi

# Define escape sequences for display attributes.  The sgr capability sets the
# nine attributes on or off.  The attributes are, in order:
#    standout, underline, reverse, blink, dim, bold, blank, protect,
#    and alternate character set
#
# The sgr0 capability turns off all attributes.
#
# Not all terminals support all atributes and even for those attributes they
# support not all combinations are necessarily supported.
reverse=""
bold=""
reset_attrib=""
if test_tput sgr0; then
    if test_tput sgr; then
	bold=`tput sgr 0 0 0 0 0 1 0 0 0`
	reverse=`tput sgr 0 0 1 0 0 0 0 0 0`
    fi
    # Note: We define reset_attribute last because sourcing this script with
    # -xv will cause these values to be printed as the variables are set.
    # Resetting last means that the net result is that we don't leave various
    # display attributes on for the rest of the script.
    reset_attrib=`tput sgr0`
fi

# If requested, print a test containing all color foreground/background
# combinations both bold and not.
if [ "${run_test:-0}" -eq 1 ]; then
    color_test() {
	local f b fg bg fn bn
        for f in black red green yellow blue magenta cyan white; do
            fg=`eval echo '${'"$f"'_fg}'`
            for b in black red green yellow blue magenta cyan white; do
	        bg=`eval echo '${'"$b"'_bg}'`
	        fn=`echo "$f" | sed 's/\(....\).*/\1/'`
	        bn=`echo "$b" | sed 's/\(....\).*/\1/'`
	        printf "%s%s%-4s%5s" "$fg" "$bg" "$fn" "$bn"
            done
            printf "%s\n" "$orig_colors"
        done
    }

    printf "%s" "$reset_attrib"
    color_test
    printf "%s" "$bold"
    color_test
    printf "%s%s" "$reset_attrib$orig_colors"
fi

# Successfully provided the colors module.
shutil_provide colors

#EOF
