#!/bin/sh
# -*- sh -*-
# Base shared shell utilities.
#
# Copyright (c) 2000-2019 Chris Sloan


###########################################################################
# Variables
###########################################################################

# prog: The basename of the script.  It is used for usage reporting, among
# other things.  Zsh does things differently, so if ZSH_SCRIPT is set, we use
# that.  All the other shells should not set it and should just use $0.
prog="$(basename -- "${ZSH_SCRIPT:-$0}")"

# default_verbose: The default verbosity level if $verbose is unset or null.
default_verbose=1

# verbose: Controls the verbosity of the script.  If unset or null, then
# treated as $default_verbose.  Generally should be tested with the is_verbose
# command for consistency.
verbose=1

# vprompt: Prefixed to the output of the various vrun type commands.  It makes
# it easier to pick the commands from the output.
vprompt="==> "

# vsuffix: Suffixed to the output of the various vrun type commands.  It is
# used to return the original terminal colors when colors are enabled.
vsuffix=""


###########################################################################
# Variable access
###########################################################################

# Indirect variable access (accessing a variable by constructing its name
# instead of having the name hard-coded) simplifies building various other
# facilities.

# set_var var_name value...
# Set the variable designated by the name passed as var_name to the
# concatenation of the values passed in.
set_var() {
    local var
    verify_args "set_var var value" 1 1000000 "$@"
    var="$1"
    [ -n "$var" ] || fatal_error "set_var: requires non-null variable name"
    shift
    eval "$var=\"\$*\""
}

# get_var var_name default_val
# Print the value of the variable given by var_name.  If it is unset or null,
# return the default value.
get_var() {
    verify_args "get_var var [default_val]" 1 2 "$@"
    [ -n "$1" ] || fatal_error "get_var: requires non-null variable name"
    eval echo '"${'"$1"':-'"$2"'}"'
}

# get_var_if_set var_name unset_val
# Print the value of the variable given by var_name.  If it is unset (but not
# if it is null), return the unset value.
get_var_if_set() {
    verify_args "get_var_if_set var [unset_val]" 1 2 "$@"
    [ -n "$1" ] || fatal_error "get_var_if_set: requires non-null variable name"
    eval echo '"${'"$1"'-'"$2"'}"'
}

# var_is_set var_name
# True if the variable specified by var_name is set.  False otherwise.
var_is_set() {
    for name in "$@"; do
	if eval [ -z "\"\${$name+set}\"" ]; then
	    return 1
	fi
    done
    return 0
}

# var_is_unset var_name
# True if the variable specified by var_name is unset.  False otherwise.
var_is_unset() {
    for name in "$@"; do
	if eval [ -n "\"\${$name+set}\"" ]; then
	    return 1
	fi
    done
    return 0
}

# var_is_null var_name
# True if the variable specified by var_name is null or unset.  False
# otherwise.
var_is_null() {
    for name in "$@"; do
	if eval [ -n "\"\${$name}\"" ]; then
	    return 1
	fi
    done
    return 0
}

# var_is_not_null var_name
# True if the variable specified by var_name is not null or unset.  False
# otherwise.
var_is_not_null() {
    for name in "$@"; do
	if eval [ -z "\"\${$name}\"" ]; then
	    return 1
	fi
    done
    return 0
}


###########################################################################
# Messages
###########################################################################

# fatal_error msg...
# Prints an error message (to stderr) and exits with error code 1.
fatal_error() {
    echo "${red_fg}$@${orig_colors}" >&2
    exit 1
}

# warning msg...
# Prints an error message (to stderr) but does not terminate the program.
warning() {
    echo "${red_fg}$@${orig_colors}" >&2
}

# fatal_error_cat [cat_args]
# Prints its stdin (or the supplied cat arguments) as an error message on
# stderr and exits with error code 1.
fatal_error_cat() {
    cat "$@" >&2
    exit 1
}

# is_verbose [level]
# Returns true if the current verbosity is at or above level.  If unspecified,
# level defaults to 1.
is_verbose() {
    local level="${1:-1}"
    if [ "${verbose:-${default_verbose:-1}}" -ge "$level" ]; then
	return 0
    else
	return 1
    fi
}

# pecho echo_args...
# Echo the supplied arguments with vprompt.
pecho() {
    echo "${vprompt}$@${vsuffix}"
}

# vecho echo_args...
# Echo its arguments only when is_verbose.
vecho() {
    if is_verbose; then
	echo "$@"
    fi
    return 0
}

# vpecho echo_args...
# vecho with vprompt.
vpecho() {
    vecho "${vprompt}$@${vsuffix}"
}

# pprintf printf_args...
# Printf with vprompt.
pprintf() {
    printf "${vprompt}"
    printf "$@"
    printf "${vsuffix}"
}

# vprintf printf_args...
# Printfs its arguments only when is_verbose.
vprintf() {
    if is_verbose; then
	printf "$@"
    fi
}

# vpprintf printf_args...
# vprintf with vprompt.
vpprintf() {
    vprintf "${vprompt}"
    vprintf "$@"
    vprintf "${vsuffix}"
}

# vechovars var_name...
# If is_verbose, print '$var_name="<var_value>"' for each var_name argument.
vechovars() {
    local var val
    for var in "$@"; do
	eval val="\"\$${var}\""
	vprintf '%s="%s"\n' "$var" "$val"
    done
}

# vargs args...
# When verbose, print a numbered list of arguments.
vargs() {
    local n=0 i
    is_verbose || return
    for i in "$@"; do
	n="$((n + 1))"
	echo "$n: \"$i\""
    done
}

# verify_args "cmd formal_arg formal_arg..." min max arg1 arg2 arg3...
# The first argument gives the command you are verifying the arguments
# for along with the names of the "formal arguments".  The min and max
# give the minimum and maximum number of arguments which will be
# accepted.  Next follow the actual arguments passed to the command
# (probably from an expansion of "$@").  If there are too many or too
# few arguments, the error message will say so and show the names of
# the formal arguments in the message as well as the actual command
# line.
verify_args() {
    local full_cmd="$1" min="$2" max="$3" cmd
    shift 3
    cmd="$(echo "$full_cmd" | cl 1)"
    if [ "$#" -ge "$min" -a "$#" -le "$max" ]; then
	return 0
    else
	fatal_error_cat <<EOF
Error:  The command: "$full_cmd"
requires a minimum of $min argument(s) and a maximum of $max argument(s).
It was called with the following $# arguments: "$cmd $@"
EOF
    fi
}


###########################################################################
# Verbose command running.
###########################################################################

# vrun cmd [args...]
# Prints a command-line (if is_verbose) and then runs it.  The command can be
# shell functions and such, but redirction apply to vrun, not to the command,
# so are often not what you want.
vrun() {
    vecho "${vprompt}$@${vsuffix}"
    "$@"
}

# vrun2 sh_cmd args...
# Like vrun, vrun2 prints a command line (if is_verbose) and then runs it, but
# it runs it with sh -c.  This helps for redirections.  You can vrun2 'ls >
# file' but doing it with vrun wouldn't work.
vrun2() {
    vecho "${vprompt}$@${vsuffix}"
    sh -c "$*"
}

# vrun3 cmd [args...]
# As with vrun and vrun2, vrun3 vechos its command line and runs it.
# vrun3 uses eval instead of sh -c, though.  If you quote your command
# the right number of times, this is more flexible than vrun2.  For
# example, one can indirectly reference variables and so on because of the
# extra evaluation.
vrun3() {
    vecho "${vprompt}$@${vsuffix}"
    eval "$@"
}

# vcd directory
# Accepts other cd arguments, too.
# Changes the current directory and vechos the pwd once it gets there.
vcd() {
    if cd "$@"; then
	vpprintf "cd %-15s (new wd=%s)\n" "$*" "$(pwd)"
    else
	vpprintf "cd %-15s failed (wd=%s)\n" "$*" "$(pwd)"
	return 1
    fi
}


###########################################################################
# Assertions
###########################################################################

# assert command args...
# Run a command.  If the command fails to return successfully, then
# report a fatal error message.  It is assumed that the assertions
# should always be true except in cases of implementation error.  In
# other words, if at some point assertions are optimized out depending
# on the type of run, then the behavior of the program should not
# change.
assert() {
    if "$@"; then
	return 0
    else
	fatal_error "*** Failed: assert $@"
    fi
}

# assert_not command args...
# Assert that the command returns false (non-zero).
assert_not() {
    if "$@"; then
	fatal_error "*** Failed: assert_not $@"
    else
	return 0
    fi
}

# assert_eq var_name exp_value
# Assert that the value of the variable var_name is equal exp_value.
# If exp_value is not supplied, then var_name's value must be unset.
# If exp_value is null, then var_name's value must be null.
# If exp_value is non-null, then var_name's value must be equal to it.
assert_eq() {
    local var_name="$1" exp_value="$2" var_is_set real_value
    verify_args "assert_eq var_name exp_value" 1 2 "$@"
    [ -n "$var_name" ] || \
	fatal_error "assert_eq: var_name empty"

    eval var_is_set="\${$var_name+set}"
    eval real_value="\${$var_name}"

    if [ -z "$var_is_set" ]; then
	if [ "$#" -eq 1 ]; then
	    return 0
	else
	    fatal_error "*** assert_eq: \"$var_name\" is unset," \
		"exp_value=\"$exp_value\""
	fi
    else
	if [ "$#" -eq 1 ]; then
	    fatal_error "*** assert_eq: \"$var_name\"=\"$real_value\"," \
		"exp_value is unset."
	fi
    fi

    # Both var_name and exp_value are set.
    if [ "$real_value" != "$exp_value" ]; then
	fatal_error "*** assert_eq: \"$var_name\"=\"$real_value\"," \
	    "exp_value=\"$exp_value\""
    fi

    return 0
}


# assert_set [var_name...]
# Asserts that supplied variables are set.
assert_set() {
    for name in "$@"; do
	eval [ -n "\"\${$name+set}\"" ] || \
	    fatal_error "*** assert_set failed on variable: $name"
    done
}

# assert_unset [var_name...]:  Asserts that supplied variables are
# not set.
assert_unset() {
    for name in "$@"; do
	eval [ -z "\"\${$name+set}\"" ] || \
	    fatal_error "*** assert_unset failed on variable: $name"
    done
}

# assert_null [var_name...]:  Asserts that supplied variables are
# either null or unset.
assert_null() {
    for name in "$@"; do
	eval [ -z "\"\${$name}\"" ] || \
	    fatal_error "*** assert_null failed on variable: $name"
    done
}

# assert_not_null [var_name...]:  Asserts that supplied variables are
# set and not null.
assert_not_null() {
    for name in "$@"; do
	eval [ -n "\"\${$name}\"" ] || \
	    fatal_error "*** assert_not_null failed on variable: $name"
    done
}

# check_success command args...
# Report a fatal error if the command fails to return success.  This
# is similar to assertion, but would never be optimized out in a
# production version of the script.
check_success() {
    if "$@"; then
	return 0
    fi
    fatal_error "*** Command failed: $@"
}


###########################################################################
# Logic functions
###########################################################################

# not cmd args...
# Invert the return code of cmd.
not() {
    if "$@"; then
	return 1
    else
	return 0
    fi
}


###########################################################################
# Paths and files
###########################################################################

# make_absolute directory...
# Print an absolute path equivalent to each argument.  Note that the resulting
# paths are not necessarily normalized to canonical form.  Also note that this
# doesn't work on most windows sh's because of the drive specifier.
make_absolute() {
    local i
    for i in "$@"; do
	case "$i" in
	    /*)
		echo "$i"
		;;
	    *)
		echo "$(pwd)/$i"
	esac
    done
}

# path_prepend dirs...
# Add paths to the beginning of PATH.  Note that this tends not to work with
# windows shells.
path_prepend() {
    local p="$1" ap
    if [ "$#" -eq 0 ]; then
	return
    fi
    shift
    path_prepend "$@"
    ap="$(make_absolute "$p")"
    PATH="$ap${PATH:+:$PATH}"
    export PATH
}

# path_append dirs...
# Add paths to the end of PATH.  Note that this tends not to work with windows
# shells.
path_append() {
    local p ap
    for p in "$@"; do
	ap="$(make_absolute "$p")"
	PATH="${PATH:+${PATH}:}$ap"
    done
    export PATH
}

# optimized_mkdir [-vp] dir1 dir2...
#     -p
#        Create non-existent parent directories too.
#     -v
#        Enable verbose messages.  If the directory is actually
#        created, we print out the mkdir command.  Otherwise we are
#        silent.
# We make directories (with mkdir) but if we are told to make the same
# directory twice in a row, we ignore the subsequent requests.  We
# also ignore it if the directory already exists.  If you have a
# script which calls mkdir or mkdir -p repeatedly on the same
# directories, this can speed it up a lot.
optimized_mkdir() {
    local i mkdir_dashp= verbose="$verbose" OPT OPTIND=1 OPTARG ret=0

    while getopts :vp OPT; do
	case "$OPT" in
	    v|+v)
		verbose=1
		;;
	    p|+p)
		mkdir_dashp="-p"
		;;
	    *)
		fatal_error "optimized_mkdir [-vp] dirs"
	esac
    done
    shift "$(expr "$OPTIND" - 1)"

    for i in "$@"; do
	if [ "$optimized_mkdir_last_directory" != "$i" ]; then
	    if [ ! -d "$i" ]; then
		if vrun mkdir $mkdir_dashp "$i"; then
		    optimized_mkdir_last_directory="$i"
		else
		    ret=1
		fi
	    fi
	fi
    done

    return "$ret"
}

# exists files...
# Return true if all files listed exist.  Note that due to portability
# concerns, we don't use -e.  It exists in ksh and many other shells,
# but not all sh varients have it.  Possibly we could try it anyway
# and just catch stderr in case it is invalid.
exists() {
    local i
    for i in "$@"; do
	[ -f "$i" ] && continue
	[ -d "$i" ] && continue
	[ -h "$i" ] && continue
	[ -c "$i" ] && continue
	[ -b "$i" ] && continue
	[ -p "$i" ] && continue
	return 1
    done

    return 0
}

# file_exists files...
# Return true if all listed files are existing regular files.
file_exists() {
    local i
    for i in "$@"; do
	[ -f "$i" ] && continue
	return 1
    done

    return 0
}

# directory_exists files...
# Return true if all listed files are existing directories.
directory_exists() {
    local i
    for i in "$@"; do
	[ -d "$i" ] && continue
	return 1
    done

    return 0
}

# in_path programs...
# Return true if all programs are available in the path.
in_path() {
    if which "$@" >/dev/null 2>&1; then
	return 0
    fi
    return 1
}

# create_tmpdir prog_name
# Create a temporary directory.
create_tmpdir() {
    local prog_name="$1"
    verify_args "create_tmpdir prog_name" 1 1 "$@"
    shift
    tmpdir="${TMP:-${TEMP:-/tmp}}/$prog_name.$$"
    if in_path mktemp; then
	tmpdir="$(mktemp -d "$tmpdir.XXXXXXXX")"
    else
	mkdir -p "$tmpdir"
    fi
    trap "cleanup_tmpdir" 0
}

# cleanup_tmpdir
# Used by create_tmpdir to clean up the tmpdir on program exit.  By making this
# a function instead of puting the rm command in the trap command, we avoid
# quoting problems.
cleanup_tmpdir() {
    rm -rf "$tmpdir"
}


###########################################################################
# Text manipulation
###########################################################################

# cl <column_numbers...>
# Prints columns of its input.  "cl 3 1" means print the third column and the
# first column in that order.  A 0 means the whole line.  Therefore, "cl 2 0"
# copies the second column in front of a copy of the line.
cl() {
    local list i

    for i in "$@"; do
	list="${list:+${list}, }\$$i"
    done

    awk "{print $list}"
}

# join_args sep args...
# Joins arguments together with the separator in between.  For example,
# "join_args , 1 2 3" gives "1,2,3".
#
# No arguments gives the empty string.  An empty separator joins the arguments
# with nothing in between.  Empty arguments will be joined with a separator.
# (That is, "join_args ',' 1 '' 2" gives "1,,2".)  Multi-character separators
# are supported.
join_args() 
{
    local sep="$1" result i
    if [ "$#" -ge 1 ]; then
	shift

	for i in "$@"; do
	    result="${result+${result}${sep}}${i}"
	done
    fi
    echo "$result"
}

# n_spaces num_spaces
# Prints the requested number of spaces.  This does not print a newline.
n_spaces() {
    printf "%${1}s" ''
}

# indent_cat num_spaces [files...]
# Indent stdin or one or more files by the specified number of spaces.
indent_cat() {
    local pre="$(n_spaces "$1")"
    [ "$#" -ge 1 ] && shift
    sed -e "s/^/$pre/" "$@"
}


###########################################################################
# Prompting and input
###########################################################################

# prompt [prompt_str [var_name [default]]]
# Prompt the user with the supplied prompt string.  Return the result in
# var_name (REPLY if unspecified).  If the response is empty, replace it with
# the default, if it was supplied.  If stdin is at EOF, then return non-zero.
prompt() {
    local prompt_str="$1" var_name="${2:-REPLY}" def="$3" ret

    printf "%s%s> " "$prompt_str" "${def:+[$def]}"
    read REPLY
    ret="$?"
    set_var "$var_name" "${REPLY:-$def}"

    return "$ret"
}

# yn [prompt_str [default]]
# Prompt the user with the supplied yes/no question.  The return code will be
# zero if they answered yes and non-zero if they answered no.  If supplied, the
# default is used if the response is blank.  If no default is supplied, then
# the default will be "yes".  Responses are not case sensitive and only check
# the first character for "y" or "n".
yn() {
    local prompt_str="$1" def="$2" REPLY
    verify_args "yn prompt_str [default]" 0 2 "$@"
    while /bin/true; do
	prompt "$prompt_str" REPLY "${def:-y}"
	case "$REPLY" in
	    [Yy]*)
		return 0;;
	    [Nn]*)
		return 1;;
	    *)
		echo "Unexpected response."
		;;
	esac
    done
}

# yn_run default cmd [args...]
# Prompt the user with a command and ask if they want to run it.  The default
# specifies the choice if the response is blank.
# The return code will be the return code of the command if it was run or 1 if
# it was not.
yn_run() {
    local def="$1"
    verify_args "yn_run default cmd args..." 2 100000 "$@"
    shift 1
    echo "== cmd: $@" 
    if yn "Run?" "$def"; then
	vrun "$@"
	return "$?"
    fi

    return 1
}


###########################################################################
# Terminal
###########################################################################

# Return 0 if stdout is a tty.
isatty() {
    # Copy stdout to stdin, then redirect stdout and stderr to
    # /dev/null.  This means that we test if stdout is a terminal but
    # we throw away the output (using only the exit status).
    if tty <&1 >/dev/null 2>&1; then
	return 0
    else
	return "$?"
    fi
}

# enable_colors [force]
# Enable colors in vrun and such if on a terminal.
#
# The terminal detection state can have three values: 0 to force colors off, 1
# to force colors on, and 2 to use colors if isatty returns true.
#
# If the "force" argument is provided, its value is used (0, 1, or 2).
# Otherwise, if shutil_colors_force is set, its value is used (0, 1, or 2).
# Otherwise, we default to detection (that is, 2).
enable_colors() {
    local force="$1" enable=
    [ -z "$enable" -a -n "$force" ] && enable="$force"
    [ -z "$enable" -a -n "$shutil_colors_force" ] && enable="$shutil_colors_force"
    [ "${enable:-2}" -gt 1 ] && isatty && enable=1
    [ "${enable:-0}" -eq 1 ] || return 1
    shutil_require colors

    vprompt="${magenta_fg}==> ${yellow_fg}"
    vsuffix="${orig_colors}"
}

######################################################################

enable_colors

shutil_provide base

#EOF
