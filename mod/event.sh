#!/bin/sh
# -*- sh -*-
# Event library.
#
# Copyright (c) 2000-2023 Chris Sloan
#
# This library provides tools for running repeating periodic events.  We
# specify a periodic event by its period and phase.  If the period is ten and
# the phase is zero, then the nominal times of the event will be at 0, 10, 20,
# 30, and so on.  A phase of 1 would result in times 1, 11, 21, 31, and so on.
#
# A simple usage example:
#
#     while sleep_until_next_event; do
#         is_event_ready my_event_1 10 && run_my_event_1
#         is_event_ready my_event_2 60 && run_my_event_2
#     done
#
# This nominally runs run_my_event_1 every 10 seconds and run_my_event_2 every
# 60 seconds.
#
# If one calls is_event_ready before the next event time, then it returns false
# indicating that one should not run the event at this time.
#
# If one calls is_event_ready after the next event time, then it returns true
# indicating that one should run the event immediately.  After returning true,
# is_event_ready doesn't return true again until after the next future event
# time.  That is, it doesn't matter how many full cycles are missed.  We always
# run as soon after the deadline as possible and then wait for the next future
# time.


shutil_require base


# SHUTIL_VIRTUAL_TIME:
#
# If unset, get_time and friends run in real time using the system clock.  
#
# If set, then this variable indicates the current virtual time.  This is the
# time returned by get_time and this time advances instantaneously during calls
# to sleep_until_next_event.


# min_time:
# The minimum representable time.
min_time=-1
while [ "$((min_time * 2))" -lt "$min_time" ]; do
    min_time="$((min_time * 2))"
done


# get_time:
# Return the current time as seconds since the epoch.
get_time() {
    if [ -n "$SHUTIL_VIRTUAL_TIME" ]; then
        # Return the virtual time.
        echo "${SHUTIL_VIRTUAL_TIME:-0}"
    else
        # Unset.
        #
        # Return the time from the system clock.
        if [ -n "$BASH" ]; then
            echo "$EPOCHSECONDS"
        else
            date +%s
        fi
    fi
}

# set_time time_sec:
# Enable virtual time and set it to `time_sec` seconds.
set_time() {
    [ -n "$1" ] || fatal_error "set_time requires an argument."
    SHUTIL_VIRTUAL_TIME="$1"
}

# advance_time delta_time_sec:
# Advance the virtual time by `delta_time_sec` seconds.  (Enables virtual time
# if not enabled.)
advance_time() {
    [ -n "$1" ] || fatal_error "advance_time requires an argument."
    SHUTIL_VIRTUAL_TIME="$(( ${SHUTIL_VIRTUAL_TIME:-0} + $1 ))"
}

# is_event_ready event_name period:
# Tool for running events on a period with a zero phase.
#
# See the library docs above for an example usage.
#
# Arguments:
# - `event_name`: used as part of a variabe name to track scheduling.
# - `period`: The period in seconds.
#
# Returns:
# - 0 if the event should be run.
# - Non-zero otherwise.
is_event_ready() {
    local event_name="$1" period="$2" now last this next
    [ -n "$event_name" ] || \
        fatal_error "is_event_ready requires an event name."
    [ -n "$period" ] || \
        fatal_error "is_event_ready requires a period."

    now="$(get_time)"

    if [ "$period" -le 0 ]; then
        # Non-positive periods mean we run as fast as possible.
        shutil_next_event="$now"
	set_var "shutil_last_event_$event_name" "$now"
	return 0
    fi

    # A positive period.
    #
    # Calculate:
    # - `last`: The last time we ran.
    # - `this`: The nominal time of this cycle.
    # - `next`: The nominal time we run next.
    last="$(get_var "shutil_last_event_$event_name" "$min_time")"
    this="$(( ( now / period ) * period ))"
    next="$(( this + period ))"

    # If the next wakeup is earlier than `shutil_next_event` update it.
    [ "$next" -lt "${shutil_next_event:-0}" ] && shutil_next_event="$next"

    # If `this` > `last`, we're at or past our deadline.  If `next` < `last`,
    # then time has wrapped or it has stepped backwards non-monotonically, so
    # we run immediately and reset our cycle.
    if [ "$this" -gt "$last" -o "$next" -lt "$last" ]; then
	set_var "shutil_last_event_$event_name" "$this"
	return 0
    fi

    # No event to run.
    return 1
}

# reset_time_until_next_event:
# Reset the time until the next event so we can start the next cycle.
reset_time_until_next_event() {
    shutil_next_event="$(( $(get_time) + 3600 ))"
}

# time_until_next_event:
# Prints the time until the next event.  If the event is late, we print "0".
#
# Returns:
# - 0 if the event is in the future (that is, we should sleep or call back
#   later).  (We also print a time of zero.)
# - 1 if the event is in the past and we should not wait to start the next
#   cycle.  (We also print a positive time in this case.)
time_until_next_event() {
    local now="$(get_time)" dt ret

    dt="$(( ${shutil_next_event:=$now} - now ))"

    if [ "$dt" -le 0 ]; then
        echo "0"
        ret=1
    else
        echo "$dt"
        ret=0
    fi

    return "$ret"
}

# sleep_until_next_event:
# If the next event is in the future, sleep until that time.  Otherwise return
# immediately.  Either way, we reset `shutil_next_event` so we can start a new
# cycle calculating the next event.
#
# When virtual time is enabled, then when the next event is in the future, this
# function advances virtual time to that point immediately.
sleep_until_next_event() {
    # Get the next event time (and reset `shutil_next_event` for the next
    # cycle).
    local dt="$(time_until_next_event)"
    if [ "$dt" -gt 0 ]; then
        # No event is due, so we need to sleep.
        if [ -n "$SHUTIL_VIRTUAL_TIME" ]; then
            # Advance time to the time of the next event.
            advance_time "$dt"
        else
            # Sleep until the time of the next event.
            sleep "$dt"
        fi
    fi

    reset_time_until_next_event
}


# backoff_run name period_1 period_2... period_n -- cmd args...
backoff_run() {
    local name="$1" now last_ret last_success_time period
    [ -n "$name" ] || \
        fatal_error "backoff_run: Name argument must be specified."
    shift

    [ -n "$*" ] || \
        fatal_error "backoff_run $name: At least one period must be specified."

    now="$(get_time)"
    last_ret="$(get_var "backoff_run_last_ret_$name" "0")"
    last_success_time="$(get_var "backoff_run_success_time_$name" "$now")"

    # Start with the first period.
    period="$1"
    shift

    if [ "$last_ret" -ne 0 ]; then
        # The last command was a failure.
        #
        # Search through the periods until we find the largest which is less
        # than the total time elapsed since the last successful run.
        since_pass="$(( now - last_success_time ))"
        while [ -n "$*" -a "$1" != "--" ]; do
            if [ "$since_pass" -ge "$1" ]; then
                period="$1"
                shift
            else
                break
            fi
        done
    fi

    # Discard the rest of the period aguments and the "--".
    while [ "$1" != "--" ]; do
        shift
    done

    [ "$1" = "--" ] && shift

    # Check if we are ready to run.
    is_event_ready "$name" "$period" || return 0

    # Run the command.
    "$@"

    last_ret="$?"
    set_var "backoff_run_last_ret_$name" "$last_ret"

    if [ "$last_ret" -eq 0 ]; then
        last_success_time="$now"
    fi
    set_var "backoff_run_success_time_$name" "$last_success_time"

    return "$last_ret"
}


shutil_provide event

#EOF
