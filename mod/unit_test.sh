#!/bin/sh
# -*- sh -*-
# Unit test utilities
#
# Copyright (c) 2000-2019 Chris Sloan

shutil_require base


# echo2 msg...:
# Echo `msg` to both stdout and stderr.
#
# Used in unit tests to make it easier to follow the synchronization of output
# in stdout and stderr.
echo2() {
    vecho "=(out)=> $*"
    vecho "=(err)=> $*" >&2
}

# check_error cmd [args...]:
# Run the command and expect it to return with an error (a non-zero return
# value).
check_error() {
    if ("$@"); then
        fatal_error "Expected error from \"$*\""
    else
        return 0
    fi
}

# vcheck_error cmd [args...]:
# Run the command and expect it to return with an error (a non-zero return
# value).  Also print message to stdout and stderr so that unit test output is
# easier to follow.
vcheck_error() {
    echo2 "vcheck_error RUN: $*"
    if ("$@"); then
        fatal_error "Expected error from \"$*\""
    else
        echo2 "    vcheck_error: PASSED (exit code = $?)"
        return 0
    fi
}

shutil_provide unit_test

#EOF
