#!/bin/sh
# -*- sh -*-
# Shared utility functions for sh scripts.
#
# Copyright (c) 2000-2023 Chris Sloan
#
# This module provides version-aware module inclusion.

###########################################################################
# Prerequisites:
# - SHUTIL_PATH must point to the directory containing this script and
#   associated files.
# - One generally invokes this script as follows:
#   - . "$SHUTIL_PATH/shutil2.sh"
# - This means you source it, not execute it directly.
#   - (Directly executing it will not define any variables or functions in the
#     parent environment.)
###########################################################################

# Display an error and exit.
shutil_error() {
    echo "Error: $*" >&2
    exit 1
}

[ -n "$SHUTIL_PATH" ] || \
    shutil_error "SHUTIL_PATH is empty or unset."
[ -d "$SHUTIL_PATH" ] || \
    shutil_error "SHUTIL_PATH is not a valid directory: $SHUTIL_PATH"

SHUTIL_PATH="$(realpath "$SHUTIL_PATH")"

#export SHUTIL_PATH

# A list of successfully loaded modules.
shutil_loaded_mod_list=

# shutil_mod_status mod: Return the status of module mod.
shutil_mod_status() {
    [ -n "$1" ] || shutil_error "Empty shutil module name."
    eval echo "\${shutil_mod_$1:-unloaded}"
}

# shutil_mod_set_status mod status: Set the status of module mod to "status".
shutil_mod_set_status() {
    [ -n "$1" ] || shutil_error "Empty shutil module name."
    eval "shutil_mod_$1"="$2"
}

# shutil_require mod: Load the specified module (if not loaded yet).
shutil_require() {
    local mod_name="$1" mod_file="$SHUTIL_PATH/mod/$1.sh" mod_status

    mod_status="$(shutil_mod_status "$mod_name")"
    case "$mod_status" in
	unloaded)
	    # Mark that we're loading to catch dependence loops.
	    shutil_mod_set_status loading

	    [ -f "$mod_file" ] || \
		shutil_error "shutil_require: Failed to find module: $mod_file"

	    # Load the module.
	    . "$mod_file"

	    # Check that the module called shutil_provide.
	    mod_status="$(shutil_mod_status "$mod_name")"
	    [ "$mod_status" = "loaded" ] || \
		shutil_error "shutil_require loading error: $mod_name"
	    ;;
	loading)
	    shutil_error "shutil_require: Loop found loading: $mod_name."
	    ;;
	loaded)
	    # Already loaded.  Nothing to do.
	    ;;
	*)
	    shutil_error "shutil_require: Unexpected \"$mod_name\" module status:" \
		"$mod_status"
	    ;;
    esac
}

# shutil_provide mod [status]: Indicate the status of a loaded module.
# See shutil_required for statuses.
shutil_provide() {
    local mod="$1" mod_status="${2:-loaded}"

    [ -n "$mod" ] || shutil_error "Empty shutil module name."
    shutil_mod_set_status "$mod" "$mod_status"
    if [ "$mod_status" = loaded ]; then
	# Append the loaded module's name to the list.
	local l="${shutil_loaded_mod_list}"
	l="${l:+$l }$mod"
	shutil_loaded_mod_list="$l"
    fi
}

# shutil_get_loaded_mod_list: Returns the list of names of loaded modules.
shutil_get_loaded_mod_list() {
    echo "$shutil_loaded_mod_list"
}

#EOF
