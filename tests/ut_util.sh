#!/bin/sh
# -*- sh -*-
# Utilities for unit testing shutil.
#
# Copyright (c) 2019 Chris Sloan


# find_free_number check_pat name_pat
# Find a free number to use to (for example) number a case name.  The first
# agument is a printf pattern of name prefixes to check while the second is a
# printf pattern of name prefixes to use once a free number has been found.
#
# Example:
#     find_free_number "case_%02d" "case_%02d_name_of_case"
#
# This will search (starting at case_00*, then case_01* and so on) until it
# finds a number which is unused.  If the first unused number was 5, then it
# would return "case_05_name_of_case".
find_free_number() {
    local check_pat="$1" name_pat="$2" n=0 check_name name
    while true; do
	check_name="$(printf "$check_pat" "$n")"
	if exists "$check_name"*; then
	    n="$((n + 1))"
	    continue
	else
	    printf "$name_pat\n" "$n"
	    return
	fi
    done

    fatal_error "Unreachable."
}

# is_unique_dir var_name dir_name...
# If there is only one dir_name argument (two arguments total) and it is a
# directory, then assign it to var_name and return 0.  Otherwise, var_name is
# left untouched and 1 is returned.  The main use of this function is to pass
# it a glob and ensure that only one directory results.
is_unique_dir() {
    local var_name="$1"
    shift
    if [ "$#" -eq 1 ]; then
	if [ -d "$1" ]; then
	    set_var "$var_name" "$1"
	    return 0
	fi
    else
	fatal_error "is_unique_dir $var_name: Too many args: $*"
    fi
    return 1
}

# is_unique_file var_name file_name...
# If there is only one file_name argument (two arguments total) and it is a
# file, then assign it to var_name and return 0.  Otherwise, var_name is left
# untouched and 1 is returned.  The main use of this function is to pass it a
# glob and ensure that only one file results.
is_unique_file() {
    local var_name="$1"
    shift
    if [ "$#" -eq 1 ]; then
	if [ -f "$1" ]; then
	    set_var "$var_name" "$1"
	    return 0
	fi
    else
	fatal_error "is_unique_file $var_name: Too many args: $*"
    fi
    return 1
}

# find_test var_name test_name
# Find the unique test specified by test_name.  We search a number of different
# patterns to find the appropriate test.  If the name matches multiple
# directories, then we return an error.
find_test() {
    local var_name="$1" tn="${2#test_}"
    # Full match.
    is_unique_dir "$var_name" "test_${tn}" && return 0
    # Specified number.
    is_unique_dir "$var_name" "test_${tn}"_* && return 0
    # Specified name without number.
    is_unique_dir "$var_name" test_[0-9][0-9]_"${tn}" && return 0
    # Specified name prefix without number.
    is_unique_dir "$var_name" test_[0-9][0-9]_"${tn}"* && return 0
    # Specified partial name without number.
    is_unique_dir "$var_name" test_*"${tn}"* && return 0

    return 1
}

# find_case var_name test_name case_name
# Search for a case name based on test_name and case_name.  The test_name must
# be the full test name, but case_name can be a subset of the name according to
# a number of patterns.  We search these patterns looking for cases.  If we
# find multiple cases which match, and error is returned.
find_case() {
    local var_name="$1" tn="${2}" cn="${3}"
    cn="${cn#case_}"
    cn="${cn%.sh}"
    # Full match.
    is_unique_file "$var_name" "${tn}/case_${cn}.sh" && return 0
    # Specified number.
    is_unique_file "$var_name" "${tn}/case_${cn}"_*.sh && return 0
    # Specified name without number.
    is_unique_file "$var_name" "${tn}/case_"[0-9][0-9]_"${cn}.sh" && return 0
    # Specified name prefix without number.
    is_unique_file "$var_name" "${tn}/case_"[0-9][0-9]_"${cn}"*.sh && return 0
    # Specified partial name without number.
    is_unique_file "$var_name" "${tn}/case_"*"${cn}"*.sh && return 0

    return 1
}

#EOF
