#!/bin/sh

SHUTIL_PATH=shutil
. ./shutil/shutil2.sh
shutil_require base
shutil_require unit_test

run() {
    # cl
    assert [ "$(echo 1 2 3 | cl)" = "1 2 3" ]
    assert [ "$(echo 1 2 3 | cl 1)" = "1" ]
    assert [ "$(echo 1 2 3 | cl 2)" = "2" ]
    assert [ "$(echo 1 2 3 | cl 3)" = "3" ]
    assert [ "$(echo 1 2 3 | cl 0)" = "1 2 3" ]
    assert [ "$(echo 1 2 3 | cl 3 1)" = "3 1" ]
    assert [ "$(echo 1 2 3 | cl 3 0)" = "3 1 2 3" ]
    assert [ "$(echo "1 2  3   " | cl 1)" = "1" ]
    assert [ "$(echo "1 2  3   " | cl 2)" = "2" ]
    assert [ "$(echo "1 2  3   " | cl 3)" = "3" ]
    assert [ "$(echo "1 2  3   " | cl 3 0)" = "3 1 2  3   " ]
    # Between the two threes are the three spaces from the $0 and the one space
    # between args for a total of 4 spaces.
    assert [ "$(echo "1 2  3   " | cl 0 3)" = "1 2  3    3" ]

    # join_args
    assert [ "$(join_args)" = "" ]
    assert [ "$(join_args '')" = "" ]
    assert [ "$(join_args '' apple)" = "apple" ]
    assert [ "$(join_args '' apple banana)" = "applebanana" ]
    assert [ "$(join_args '' apple banana cherry)" = "applebananacherry" ]
    assert [ "$(join_args ',')" = "" ]
    assert [ "$(join_args ',' 1)" = "1" ]
    assert [ "$(join_args ',' 1 2)" = "1,2" ]
    assert [ "$(join_args ',' 1 2 "3 4")" = "1,2,3 4" ]
    assert [ "$(join_args ',' 1 2 "3 4" '')" = "1,2,3 4," ]
    assert [ "$(join_args ',' '')" = "" ]
    assert [ "$(join_args ',' '' '')" = "," ]
    assert [ "$(join_args ',' '' '' '')" = ",," ]
    assert [ "$(join_args 'sep' '' '' '')" = "sepsep" ]
    assert [ "$(join_args ' and ' a b c)" = "a and b and c" ]

    # n_spaces
    assert [ "$(n_spaces; echo ':')" = ":" ]
    assert [ "$(n_spaces ''; echo ':')" = ":" ]
    assert [ "$(n_spaces 0; echo ':')" = ":" ]
    assert [ "$(n_spaces 1; echo ':')" = " :" ]
    assert [ "$(n_spaces 2; echo ':')" = "  :" ]
    assert [ "$(n_spaces 3; echo ':')" = "   :" ]
    assert [ "$(n_spaces 10; echo ':')" = "          :" ]

    # indent_cat
    assert [ "$(printf ':\n;\n' | indent_cat)" = "$(printf ':\n;\n')" ]
    assert [ "$(printf ':\n;\n' | indent_cat '')" = "$(printf ':\n;\n')" ]
    assert [ "$(printf ':\n;\n' | indent_cat 0)" = "$(printf ':\n;\n')" ]
    assert [ "$(printf ':\n;\n' | indent_cat 1)" = "$(printf ' :\n ;\n')" ]
    assert [ "$(printf ':\n;\n' | indent_cat 8)" = \
	"$(printf '        :\n        ;\n')" ]
    printf 'indent_test 1\nindent_test 2\n' > indent_cat.in
    indent_cat 8 indent_cat.in
}

run
set -e
run

echo "Test complete: $prog"
