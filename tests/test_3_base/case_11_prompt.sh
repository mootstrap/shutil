#!/bin/sh

SHUTIL_PATH=shutil
. ./shutil/shutil2.sh
shutil_require base
shutil_require unit_test

# test_prompt in_str prompt_args...
# Run the prompt function with the supplied arguements and supply in_str to it
# on its stdin.  If in_str is "EOF" then supply /dev/null on its stdin.
# We assume the expected return code is non-zero for EOF and zero otherwise.
test_prompt() {
    local ret f in_str="$1"
    if [ "$in_str" = "EOF" ]; then
	f=/dev/null
    else
	f=prompt_test.in
	echo "$in_str" > "$f"
    fi
    shift
    vpecho "Testing (in_str=\"$in_str\"): prompt $@"
    prompt "$@" <"$f"
    ret="$?"
    echo ""
    vechovars "${2:-REPLY}" ret
    if [ "$in_str" = "EOF" ]; then
	assert [ "$ret" -eq 1 ]
    else
	assert [ "$ret" -eq 0 ]
    fi
    return "$ret"
}

# test_cmd exp_ret in_str cmd args...
# Run the test command cmd with the supplied arguments.  in_str will be
# provided on its stdin (unless in_str is "EOF", in which case /dev/null will
# be provided).  The exp_ret argument is the expected return code from the
# command.
test_cmd() {
    local exp_ret="$1" in_str="$2" ret f
    shift 2
    if [ "$in_str" = "EOF" ]; then
	f=/dev/null
    else
	f=prompt_test.in
	echo "$in_str" > "$f"
    fi
    vpecho "Testing (in_str=\"$in_str\"): $@"
    "$@" <"$f"
    ret="$?"
    echo ""
    vechovars ret
    assert_eq ret "$exp_ret"
    return "$ret"
}

run() {
    # prompt
    assert [ "$(prompt </dev/null)" = "> " ]
    assert [ "$(prompt "Q?" </dev/null)" = "Q?> " ]
    assert [ "$(prompt "Q?" REPLY </dev/null)" = "Q?> " ]
    assert [ "$(prompt "Q?" result </dev/null)" = "Q?> " ]
    assert [ "$(prompt "Q?" REPLY y </dev/null)" = "Q?[y]> " ]

    vpecho 'EOF.  Returns default and return code 1.'
    test_prompt EOF
    assert_eq REPLY ''
    test_prompt EOF "Q?"
    assert_eq REPLY ''
    test_prompt EOF "Q?" REPLY
    assert_eq REPLY ''
    vpecho 'Verify that $result is overwritten.'
    result=1
    test_prompt EOF "Q?" result
    assert_eq result ''
    vpecho 'Verify we return the default at EOF.'
    test_prompt EOF "Q?" result y
    assert_eq result 'y'
    vpecho 'Verify again that $result is overwritten.'
    test_prompt EOF "Q?" result
    assert_eq result ''

    vpecho 'Verify empty string is treated as the default.'
    test_prompt '' "Q?" result
    assert_eq result ''
    test_prompt '' "Q?" result y
    assert_eq result 'y'

    vpecho 'Feed in responses.'
    test_prompt 'y' "Q?" result
    assert_eq result 'y'
    test_prompt 'n' "Q?" result y
    assert_eq result 'n'
    test_prompt 'y' "Q?" result y
    assert_eq result 'y'

    vpecho 'Feed in responses.  Use REPLY as variable.'
    test_prompt 'y' "Q?" REPLY
    assert_eq REPLY 'y'
    test_prompt 'n' "Q?" REPLY y
    assert_eq REPLY 'n'
    test_prompt 'y' "Q?" REPLY y
    assert_eq REPLY 'y'

    ######################################################################
    # yn
    test_cmd 0 'EOF' yn
    test_cmd 0 'EOF' yn 'Do it?'
    test_cmd 0 'EOF' yn 'Do it?' y
    test_cmd 1 'EOF' yn 'Do it?' n

    test_cmd 0 '' yn
    test_cmd 0 '' yn 'Do it?'
    test_cmd 0 '' yn 'Do it?' y
    test_cmd 1 '' yn 'Do it?' n

    test_cmd 0 'y' yn
    test_cmd 0 'y' yn 'Do it?'
    test_cmd 0 'y' yn 'Do it?' y
    test_cmd 0 'y' yn 'Do it?' n

    test_cmd 1 'n' yn
    test_cmd 1 'n' yn 'Do it?'
    test_cmd 1 'n' yn 'Do it?' y
    test_cmd 1 'n' yn 'Do it?' n

    test_cmd 0 'Y' yn
    test_cmd 0 'Y' yn 'Do it?'
    test_cmd 0 'Y' yn 'Do it?' y
    test_cmd 0 'Y' yn 'Do it?' n

    test_cmd 1 'N' yn
    test_cmd 1 'N' yn 'Do it?'
    test_cmd 1 'N' yn 'Do it?' y
    test_cmd 1 'N' yn 'Do it?' n

    vpecho 'Testing an invalid entry followed by EOF or a valid on.'
    test_cmd 0 "$(printf "invalid\n")" yn 'Do it?'
    test_cmd 0 "$(printf "invalid\ny\n")" yn 'Do it?'
    test_cmd 1 "$(printf "invalid\nn\n")" yn 'Do it?'

    vpecho 'Only the first character of the answer matters.'
    test_cmd 0 "yn" yn 'Do it?'
    test_cmd 1 "ny" yn 'Do it?'

    ######################################################################
    # yn_run
    test_cmd 0 'EOF' yn_run y true
    test_cmd 1 'EOF' yn_run y false
    test_cmd 1 'EOF' yn_run n true
    test_cmd 1 'EOF' yn_run n false

    test_cmd 0 '' yn_run y true
    test_cmd 1 '' yn_run y false
    test_cmd 1 '' yn_run n true
    test_cmd 1 '' yn_run n false

    test_cmd 0 'y' yn_run y true
    test_cmd 1 'y' yn_run y false
    test_cmd 0 'y' yn_run n true
    test_cmd 1 'y' yn_run n false

    test_cmd 1 'n' yn_run y true
    test_cmd 1 'n' yn_run y false
    test_cmd 1 'n' yn_run n true
    test_cmd 1 'n' yn_run n false

}

# Normally we rerun with set -e, but these functions intentionally return
# non-zero return codes while also producing output and setting variables using
# eval, which makes that hard to test with set -e.  In a set -e program, these
# functions would either be called in an if (or similar situation, see
# additional information below) which effectively disables set -e, or they
# would be unusable, so not testing set -e is not a major issue.
#
# Additional information: The bash man page has a good explanation of where the
# -e flag is ignored.  See the -e option under the set command for details.

run
#set -e
#run

echo "Test complete: $prog"
