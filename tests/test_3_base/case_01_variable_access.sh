#!/bin/sh

SHUTIL_PATH=shutil
. ./shutil/shutil2.sh
shutil_require base
shutil_require unit_test

run() {
    # set_var
    local apple banana cucumber date eggplant v

    set_var apple one
    assert [ "$apple" = "one" ]

    v=apple
    set_var "$v" two
    assert [ "$apple" = "two" ]

    set_var "banana" three
    assert [ "$banana" = "three" ]

    v=banana
    set_var "$v" four
    assert [ "$banana" = "four" ]

    set_var "cucumber"
    assert [ "${cucumber-unset}" = "" ]

    set_var "date" ''
    assert [ "${date-unset}" = "" ]

    check_error set_var '' 'five'

    # get_var
    assert [ "$(get_var apple)" = "two" ]
    assert [ "$(get_var apple is_null)" = "two" ]

    assert [ "$(get_var banana)" = "four" ]
    assert [ "$(get_var banana is_null)" = "four" ]

    assert [ "$(get_var cucumber)" = "" ]
    assert [ "$(get_var cucumber is_null)" = "is_null" ]

    assert [ "$(get_var date)" = "" ]
    assert [ "$(get_var date is_null)" = "is_null" ]

    assert [ "$(get_var eggplant)" = "" ]
    assert [ "$(get_var eggplant is_null)" = "is_null" ]

    v=apple
    assert [ "$(get_var "$v" "is_null")" = "two" ]
    v=banana
    assert [ "$(get_var "$v" "is_null")" = "four" ]
    v=cucumber
    assert [ "$(get_var "$v" "is_null")" = "is_null" ]
    v=date
    assert [ "$(get_var "$v" "is_null")" = "is_null" ]
    v=eggplant
    assert [ "$(get_var "$v" "is_null")" = "is_null" ]
    v=
    assert [ "$(check_error get_var "$v" "is_null")" = "" ]


    # get_var_if_set
    assert [ "$(get_var_if_set apple)" = "two" ]
    assert [ "$(get_var_if_set apple is_unset)" = "two" ]

    assert [ "$(get_var_if_set banana)" = "four" ]
    assert [ "$(get_var_if_set banana is_unset)" = "four" ]

    assert [ "$(get_var_if_set cucumber)" = "" ]
    assert [ "$(get_var_if_set cucumber is_unset)" = "" ]

    assert [ "$(get_var_if_set date)" = "" ]
    assert [ "$(get_var_if_set date is_unset)" = "" ]

    assert [ "$(get_var_if_set eggplant)" = "" ]
    assert [ "$(get_var_if_set eggplant is_unset)" = "is_unset" ]

    v=apple
    assert [ "$(get_var_if_set "$v" "is_unset")" = "two" ]
    v=banana
    assert [ "$(get_var_if_set "$v" "is_unset")" = "four" ]
    v=cucumber
    assert [ "$(get_var_if_set "$v" "is_unset")" = "" ]
    v=date
    assert [ "$(get_var_if_set "$v" "is_unset")" = "" ]
    v=eggplant
    assert [ "$(get_var_if_set "$v" "is_unset")" = "is_unset" ]
    v=
    assert [ "$(check_error get_var_if_set "$v" "is_unset")" = "" ]

    
    ######################################################################


    local us n='' s='value' us2 n2='' s2='other value'

    # var_is_set
    assert var_is_set
    assert var_is_set n s
    assert var_is_set n
    assert var_is_set s
    assert_not var_is_set us us2
    assert_not var_is_set us
    assert_not var_is_set us2

    assert_not var_is_set s us
    assert_not var_is_set us n

    # var_is_unset
    assert var_is_unset
    assert_not var_is_unset n s
    assert_not var_is_unset n
    assert_not var_is_unset s
    assert var_is_unset us us2
    assert var_is_unset us
    assert var_is_unset us2

    assert_not var_is_unset s us
    assert_not var_is_unset us n

    # var_is_null
    assert var_is_null
    assert var_is_null n us
    assert var_is_null n
    assert var_is_null us
    assert_not var_is_null s s2
    assert_not var_is_null s
    assert_not var_is_null s2

    assert_not var_is_null us s
    assert_not var_is_null s n

    # var_is_not_null
    assert var_is_not_null
    assert_not var_is_not_null n us
    assert_not var_is_not_null n
    assert_not var_is_not_null us
    assert var_is_not_null s s2
    assert var_is_not_null s
    assert var_is_not_null s2

    assert_not var_is_not_null us s
    assert_not var_is_not_null s n

}

run
set -e
run

echo "Test complete: $prog"
