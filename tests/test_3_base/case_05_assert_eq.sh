#!/bin/sh

SHUTIL_PATH=shutil
. ./shutil/shutil2.sh
shutil_require base
shutil_require unit_test

run() {
    # No args and empty args.
    vrun check_error assert_eq
    vrun check_error assert_eq ''

    # Comparing an unset variable.
    vrun unset foo

    vrun assert_eq foo
    vrun check_error assert_eq foo ''
    vrun check_error assert_eq foo qux
    vrun check_error assert_eq foo quux

    # Comparing a null variable.
    bar=

    vrun check_error assert_eq bar
    vrun assert_eq bar ''
    vrun check_error assert_eq bar qux
    vrun check_error assert_eq bar quux

    # Comparing a set variable.
    baz=qux

    vrun check_error assert_eq baz
    vrun check_error assert_eq baz ''
    vrun assert_eq baz qux
    vrun check_error assert_eq baz quux
}

vrun run
vrun set -e
vrun run

echo "Test complete."
