#!/bin/sh

SHUTIL_PATH=shutil
. ./shutil/shutil2.sh
shutil_require base
shutil_require unit_test

run() {
    check_success true
    assert [ "$(check_error check_success false 2>&1)" = \
	"*** Command failed: false" ]
}

run
set -e
run

echo "Test complete: $prog"
