#!/bin/sh

# Force colors off during initial loading.
shutil_colors_force=0

SHUTIL_PATH=shutil
. "$SHUTIL_PATH/shutil2.sh"
shutil_require base
shutil_require unit_test

# color_test exp force shutil_colors_force use_tty
# exp: 1 if colors should be loaded, 0 if not.
# force: '', 0, 1, 2 for unset, disable, enable, auto.
# shutil_colors_force: '', 0, 1, 2 for unset, disable, enable, auto.
# use_tty: 0 for /dev/null, 1 for /dev/tty.
color_test() {
    (
	local exp="$1" force="$2" shutil_colors_force="$3" use_tty="$4" stdout
	vpecho "color_test '$exp' '$force' '$shutil_colors_force' '$use_tty'"

	if [ "$use_tty" -eq 1 ]; then
	    stdout=/dev/tty
	else
	    stdout=/dev/null
	fi

	if [ "$exp" -eq 0 ]; then
	    assert_not enable_colors $force >"$stdout"
	    assert_unset red_fg
	else
	    assert enable_colors $force >"$stdout"
	    assert_set red_fg
	fi
    )
}

run() {
    vpecho "Run starting."

    # isatty

    # We assume that /dev/tty will work for the unit tests and be a terminal.
    assert_not isatty >/dev/null
    assert isatty >/dev/tty

    # enable_colors

    # We use subshell so we can run multiple tests without changing the initial
    # state.  We redirect stdout so we can control whether isatty returns true
    # or not.  We use red_fg as a test for whether colors have been enabled.

    # Stop forcing colors not to be loaded.
    unset shutil_colors_force

    # No colors should currently be loaded.
    assert_eq red_fg

    # color_test exp force shutil_colors_force use_tty
    color_test 0 '' '' 0
    color_test 1 '' '' 1
    color_test 0 '' 0 0
    color_test 0 '' 0 1
    color_test 1 '' 1 0
    color_test 1 '' 1 1
    color_test 0 '' 2 0
    color_test 1 '' 2 1

    color_test 0 0 '' 0
    color_test 0 0 '' 1
    color_test 0 0 0 0
    color_test 0 0 0 1
    color_test 0 0 1 0
    color_test 0 0 1 1
    color_test 0 0 2 0
    color_test 0 0 2 1

    color_test 1 1 '' 0
    color_test 1 1 '' 1
    color_test 1 1 0 0
    color_test 1 1 0 1
    color_test 1 1 1 0
    color_test 1 1 1 1
    color_test 1 1 2 0
    color_test 1 1 2 1

    color_test 0 2 '' 0
    color_test 1 2 '' 1
    color_test 0 2 0 0
    color_test 1 2 0 1
    color_test 0 2 1 0
    color_test 1 2 1 1
    color_test 0 2 2 0
    color_test 1 2 2 1

}

run
set -e
run

echo "Test complete: $prog"
