#!/bin/sh

SHUTIL_PATH=shutil
. ./shutil/shutil2.sh
shutil_require base
shutil_require unit_test

run() {
    # make_absolute
    (
	# Move to a known directory.
	cd /tmp

	assert [ "$(make_absolute)" = "" ]
	assert [ "$(make_absolute '')" = "/tmp/" ]
	assert [ "$(make_absolute .)" = "/tmp/." ]
	assert [ "$(make_absolute foo)" = "/tmp/foo" ]
	assert [ "$(make_absolute /foo)" = "/foo" ]
	assert [ "$(make_absolute //foo)" = "//foo" ]
	assert [ "$(make_absolute ../foo)" = "/tmp/../foo" ]
	assert [ "$(make_absolute "foo bar")" = "/tmp/foo bar" ]
    )

    # path_prepend
    (
	# cd to /tmp to make make_absolute (called inside path_prepend)
	# predictable.
	cd /tmp

	PATH=/usr/bin:/bin
	path_prepend
	assert_eq PATH "/usr/bin:/bin"

	PATH=/usr/bin:/bin
	path_prepend ''
	assert_eq PATH "/tmp/:/usr/bin:/bin"

	PATH=/usr/bin:/bin
	path_prepend /usr/local/bin
	assert_eq PATH "/usr/local/bin:/usr/bin:/bin"

	PATH=
	path_prepend /usr/bin
	assert_eq PATH "/usr/bin"

	PATH=/usr/bin:/bin
	path_prepend /usr/local/bin:/opt/bin:/usr/bin
	assert_eq PATH "/usr/local/bin:/opt/bin:/usr/bin:/usr/bin:/bin"

	PATH=/usr/bin:/bin
	path_prepend foo bar
	assert_eq PATH "/tmp/foo:/tmp/bar:/usr/bin:/bin"

	# Spaces in a path name.
	PATH=/usr/bin:/bin
	path_prepend "foo bar"
	assert_eq PATH "/tmp/foo bar:/usr/bin:/bin"

    )

    # path_append
    (
	# cd to /tmp to make make_absolute (called inside path_append)
	# predictable.
	cd /tmp

	PATH=/usr/bin:/bin
	path_append
	assert_eq PATH "/usr/bin:/bin"

	PATH=/usr/bin:/bin
	path_append ''
	assert_eq PATH "/usr/bin:/bin:/tmp/"

	PATH=/usr/bin:/bin
	path_append /usr/local/bin
	assert_eq PATH "/usr/bin:/bin:/usr/local/bin"

	PATH=
	path_append /usr/bin
	assert_eq PATH "/usr/bin"

	PATH=/usr/bin:/bin
	path_append /usr/local/bin:/opt/bin:/usr/bin
	assert_eq PATH "/usr/bin:/bin:/usr/local/bin:/opt/bin:/usr/bin"

	PATH=/usr/bin:/bin
	path_append foo bar
	assert_eq PATH "/usr/bin:/bin:/tmp/foo:/tmp/bar"

	# Spaces in a path name.
	PATH=/usr/bin:/bin
	path_append "foo bar"
	assert_eq PATH "/usr/bin:/bin:/tmp/foo bar"

    )

    # optimized_mkdir
    mkdir om_test
    (
	cd om_test

	assert [ "$(optimized_mkdir 1 2>&1)" = "==> mkdir 1" ]
	assert directory_exists 1

	# Second time, it should be silent.
	assert [ "$(optimized_mkdir 1 2>&1)" = "" ]
	assert directory_exists 1

	assert [ "$(optimized_mkdir 1/2 2>&1)" = "==> mkdir 1/2" ]
	assert directory_exists 1 1/2

	assert [ "$(optimized_mkdir 1/2 2>&1)" = "" ]
	assert directory_exists 1 1/2

	# Don't create the parent first so it fails without -p.
	assert_not optimized_mkdir 3/4 >/dev/null 2>&1
	assert not directory_exists 3 3/4

	assert_not optimized_mkdir 3/4 >/dev/null 2>&1
	assert not directory_exists 3 3/4

	# Use -p to create the parent at the same time.
	assert [ "$(optimized_mkdir -p 5/6 2>&1)" = "==> mkdir -p 5/6" ]
	assert directory_exists 5 5/6

	assert [ "$(optimized_mkdir 5/6 2>&1)" = "" ]
	assert directory_exists 5 5/6

	# Create a directory, delete it and then try to create it again.  It
	# will skip it because it doesn't realize it was deleted.
	verbose=0
	optimized_mkdir 7
	assert directory_exists 7
	rmdir 7
	optimized_mkdir 7
	assert_not directory_exists 7

	# Now we create different directory so that when we try to create 7
	# again, it will actually try.
	optimized_mkdir 8
	optimized_mkdir 7
	assert directory_exists 7 8

	# Test -v
	assert [ "$(optimized_mkdir 9 2>&1)" = "" ]
	assert [ "$(optimized_mkdir -v 10 2>&1)" = "==> mkdir 10" ]
    )
    rm -rf om_test


    # exists, file_exists, directory_exists
    mkdir ex_test
    (
	cd ex_test

	touch ex_f
	mkdir ex_d
	ln -s foo ex_link
	mkfifo ex_pipe

	ln -s ex_f ex_link_f
	ln -s ex_d ex_link_d
	ln -s no_ex_f no_ex_link_f

	# exists
	assert exists ex_f ex_d
	assert_not exists ex_f no_ex_d
	assert exists ex_f
	assert exists ex_d
	assert exists ex_link
	assert exists /dev/tty
	assert exists ex_pipe
	assert_not exists no_ex_f

	assert exists ex_link_f
	assert exists ex_link_d
	assert exists no_ex_link_f

	# file_exists
	assert_not file_exists ex_f ex_d
	assert_not file_exists ex_f no_ex_d
	assert file_exists ex_f
	assert_not file_exists ex_d
	assert_not file_exists ex_link
	assert_not file_exists /dev/tty
	assert_not file_exists ex_pipe
	assert_not file_exists no_ex_f

	assert file_exists ex_link_f
	assert_not file_exists ex_link_d
	assert_not file_exists no_ex_link_f

	# directory_exists
	assert_not directory_exists ex_f ex_d
	assert_not directory_exists ex_d no_ex_f
	assert_not directory_exists ex_f
	assert directory_exists ex_d
	assert_not directory_exists ex_link
	assert_not directory_exists /dev/tty
	assert_not directory_exists ex_pipe
	assert_not directory_exists no_ex_f

	assert_not directory_exists ex_link_f
	assert directory_exists ex_link_d
	assert_not directory_exists no_ex_link_f

    )
    rm -rf ex_test

    # in_path
    assert in_path true
    assert in_path false
    assert in_path true false
    assert_not in_path nonexistant
    assert_not in_path true false nonexistant

    # create_tmpdir: In order not to override the state of create_tmpdir in
    # this shell, we use nested subshells for these tests.
    mkdir ct_test
    (
	cd ct_test

	# Give the subshell a place to create its tmpdir and a file to tell us
	# where it created it.
	mkdir tmp
	created_dir="$(make_absolute created_dir)"
	TMP="$(make_absolute tmp)"

	(
	    # Run create_tmpdir, record where we created it and verify it
	    # exists.
	    create_tmpdir "$prog"
	    assert directory_exists "$tmpdir"
	    echo "$tmpdir" > "$created_dir"
	    assert directory_exists "$(cat "$created_dir")"
	)

	# Verify that the subshell deleted the tmpdir on exit.
	assert_not directory_exists "$(cat "$created_dir")"
    )
    rm -rf ct_test
}

run
set -e
run

echo "Test complete: $prog"
