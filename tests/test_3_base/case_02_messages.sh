#!/bin/sh

SHUTIL_PATH=shutil
. ./shutil/shutil2.sh
shutil_require base
shutil_require unit_test

run() {
    vrun check_error fatal_error "Error message."
    vrun warning "Warning message."
    vrun check_error fatal_error_cat <<EOF
Multiple line
error message.

EOF

    verbose=
    assert is_verbose

    verbose=0
    assert_not is_verbose

    verbose=1
    assert is_verbose

    verbose=2
    assert is_verbose

    verbose=
    assert_not is_verbose 2

    verbose=0
    assert_not is_verbose 2

    verbose=1
    assert_not is_verbose 2

    verbose=2
    assert is_verbose 2

    ######################################################################
    verbose=1

    assert [ "$(pecho 'pecho zero')" = "==> pecho zero" ]

    assert [ "$(vecho 'vecho one')" = "vecho one" ]
    assert [ "$(verbose=0 vecho 'vecho two')" = "" ]

    assert [ "$(vpecho 'vpecho three')" = "==> vpecho three" ]
    assert [ "$(verbose=0 vpecho 'vpecho four')" = "" ]

    assert [ "$(pprintf 'pprintf %s' 'five')" = "==> pprintf five" ]
    assert [ "$(verbose=0 pprintf 'pprintf %s' 'six')" = "==> pprintf six" ]

    assert [ "$(vprintf 'vprintf %s' 'seven')" = "vprintf seven" ]
    assert [ "$(verbose=0 vprintf 'vprintf %s' 'eight')" = "" ]

    assert [ "$(vpprintf 'vpprintf %s' 'nine')" = "==> vpprintf nine" ]
    assert [ "$(verbose=0 vpprintf 'vpprintf %s' 'ten')" = "" ]

    dog=spot
    cat=felix
    assert [ "$(vechovars dog)" = 'dog="spot"' ]
    assert [ "$(verbose=0 vechovars dog)" = "" ]
    assert [ "$(vechovars cat dog)" = 'cat="felix"
dog="spot"' ]
    assert [ "$(verbose=0 vechovars cat dog)" = "" ]


    assert [ "$(vargs 'eleven' 'twelve')" = \
	"$(printf '1: "eleven"\n2: "twelve"')" ]
    assert [ "$(verbose=0 vargs 'thirteen' 'fourteen')" = "" ]

    ######################################################################

    verify_args_run() {
	verify_args "cmd arg1 arg2" 1 2 "$@"
	return 0
    }

    act="$(check_error verify_args_run 2>&1)"
    exp='Error:  The command: "cmd arg1 arg2"
requires a minimum of 1 argument(s) and a maximum of 2 argument(s).
It was called with the following 0 arguments: "cmd "'
    assert [ "$act" = "$exp" ]
    
    act="$(verify_args_run one 2>&1)"
    exp=""
    assert [ "$act" = "$exp" ]
    
    act="$(verify_args_run one two 2>&1)"
    exp=""
    assert [ "$act" = "$exp" ]
    
    act="$(check_error verify_args_run one two three 2>&1)"
    exp='Error:  The command: "cmd arg1 arg2"
requires a minimum of 1 argument(s) and a maximum of 2 argument(s).
It was called with the following 3 arguments: "cmd one two three"'
    assert [ "$act" = "$exp" ]

    ######################################################################

    return 0
}

vrun run
set -e
vrun run

echo "Test complete: $prog"
