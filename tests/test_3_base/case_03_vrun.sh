#!/bin/sh

SHUTIL_PATH=shutil
. ./shutil/shutil2.sh
shutil_require base
shutil_require unit_test

tests() {
    assert [ "$(vrun true)" = "==> true" ]
    assert [ "$(verbose=0 vrun true)" = "" ]

    assert [ "$(check_error vrun false)" = "==> false" ]
    assert [ "$(verbose=0 check_error vrun false)" = "" ]
    
    vrun2 'echo hi >out.case_vrun'
    assert [ "$(cat out.case_vrun)" = "hi" ]

    func() {
	echo func running
    }

    vrun3 func

    foo=bar
    bar=2
    vrun3 "val=\"\$${foo}\""
    assert_eq val 2


    mkdir -p dir/1/2
    (
	# vcd prints full paths which include changing names in the tmp dir.
	# As such, we disable them for now so the output file is not
	# non-deterministic.
	verbose=0
	vcd dir
	vcd 1
	vcd ../..
	vcd dir/1/2
	)

    return 0
}

vrun tests
set -e
vrun tests

echo "Test complete: $prog"
