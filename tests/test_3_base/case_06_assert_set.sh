#!/bin/sh

SHUTIL_PATH=shutil
. ./shutil/shutil2.sh
shutil_require base
shutil_require unit_test

run() {
    local us n='' s='value'

    # assert_set
    assert [ "$(assert_set 2>&1)" = "" ]
    assert [ "$(check_error assert_set us 2>&1)" = \
	"*** assert_set failed on variable: us" ]
    assert [ "$(assert_set n 2>&1)" = "" ]
    assert [ "$(assert_set s 2>&1)" = "" ]

    # assert_unset
    assert [ "$(assert_unset 2>&1)" = "" ]
    assert [ "$(assert_unset us 2>&1)" = "" ]
    assert [ "$(check_error assert_unset n 2>&1)" = \
	"*** assert_unset failed on variable: n" ]
    assert [ "$(check_error assert_unset s 2>&1)" = \
	"*** assert_unset failed on variable: s" ]

    # assert_null
    assert [ "$(assert_null 2>&1)" = "" ]
    assert [ "$(assert_null us 2>&1)" = "" ]
    assert [ "$(assert_null n 2>&1)" = "" ]
    assert [ "$(assert_null s 2>&1)" = \
	"*** assert_null failed on variable: s" ]

    # assert_not_null
    assert [ "$(assert_not_null 2>&1)" = "" ]
    assert [ "$(check_error assert_not_null us 2>&1)" = \
	"*** assert_not_null failed on variable: us" ]
    assert [ "$(check_error assert_not_null n 2>&1)" = \
	"*** assert_not_null failed on variable: n" ]
    assert [ "$(assert_not_null s 2>&1)" = "" ]
}

run
set -e
run

echo "Test complete: $prog"
