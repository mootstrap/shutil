#!/bin/sh

SHUTIL_PATH=shutil
. ./shutil/shutil2.sh
shutil_require base
shutil_require unit_test

vrun set +e

vrun assert true
vrun check_error assert false

vrun check_error assert_not true
vrun assert_not false

vrun set -e

vrun assert true
vrun check_error assert false

vrun check_error assert_not true
vrun assert_not false

echo "Test complete"
