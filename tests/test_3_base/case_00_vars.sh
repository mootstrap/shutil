#!/bin/sh

SHUTIL_PATH=shutil
. ./shutil/shutil2.sh
shutil_require base
shutil_require unit_test

assert_eq prog case_00_vars
assert_eq default_verbose 1
assert_eq verbose 1

echo "Test complete: $prog"
