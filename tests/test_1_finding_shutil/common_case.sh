#!/bin/sh

# Get the script name.  Strip the .sh suffix if present.  (run_tests strips it
# in the run directory.)
script="$(basename "$0" .sh)"

# Extract the case number.
num="${script#case_}"
num="${num%%_*}"
num="${num%%.*}"

cd "c${num}"

. ./env
"$SCRIPT" "$@"
