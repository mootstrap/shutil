#!/bin/sh

SHUTIL_PATH=shutil
. ./shutil/shutil2.sh

# Mark the base module with a status.
shutil_provide base funky

# Check that status.
mod_status="$(shutil_mod_status base)"
[ "${mod_status}" = funky ] || \
    fatal_error "Bad module status"
