#!/bin/sh

SHUTIL_PATH=shutil
. ./shutil/shutil2.sh

# If we claim we have provided base, then requiring base will not do anything.
shutil_provide base
shutil_require base

# vprompt is defined in base, so will not exist.
echo "vprompt=${vprompt:-unset}"
