#!/bin/sh

SHUTIL_PATH=shutil
. ./shutil/shutil2.sh

# Mark the base module as having an error.
shutil_provide base error

# Requiring it will report an error.
shutil_require base
