#!/bin/sh

SHUTIL_PATH=shutil
. ./shutil/shutil2.sh

[ "$(shutil_mod_status base)" = unloaded ] && \
    [ "$(shutil_mod_status default)" = unloaded ] && \
    [ "$(shutil_mod_status unknown)" = unloaded ] || \
    echo "Unloaded module statuses are wrong."

shutil_require base

[ "$(shutil_mod_status base)" = loaded ] && \
    [ "$(shutil_mod_status default)" = unloaded ] && \
    [ "$(shutil_mod_status unknown)" = unloaded ] || \
    echo "Unloaded module statuses are wrong."

shutil_require default

[ "$(shutil_mod_status base)" = loaded ] && \
    [ "$(shutil_mod_status default)" = loaded ] && \
    [ "$(shutil_mod_status unknown)" = unloaded ] || \
    echo "Unloaded module statuses are wrong."

# Requiring a module twice has no effect.
shutil_require default
shutil_require base

[ "$(shutil_mod_status base)" = loaded ] && \
    [ "$(shutil_mod_status default)" = loaded ] && \
    [ "$(shutil_mod_status unknown)" = unloaded ] || \
    echo "Unloaded module statuses are wrong."


# We can't easily test shutil_require on an non-existant module because the
# error message contains a non-constant path.

shutil_mod_set_status unknown loaded

[ "$(shutil_mod_status base)" = loaded ] && \
    [ "$(shutil_mod_status default)" = loaded ] && \
    [ "$(shutil_mod_status unknown)" = loaded ] || \
    echo "Unloaded module statuses are wrong."

# However, now that we claim to have loaded the "unknown" module, we can
# require it again.
shutil_require unknown

[ "$(shutil_mod_status base)" = loaded ] && \
    [ "$(shutil_mod_status default)" = loaded ] && \
    [ "$(shutil_mod_status unknown)" = loaded ] || \
    echo "Unloaded module statuses are wrong."

# Test loops by faking one.  This will terminate the script with an error.
shutil_mod_set_status loop loading
shutil_require loop
