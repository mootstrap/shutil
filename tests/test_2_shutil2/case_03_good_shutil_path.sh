#!/bin/sh

SHUTIL_PATH=shutil
. ./shutil/shutil2.sh

case "$SHUTIL_PATH" in
    /*)
	;;
    *)
	print "Expected absolute SHUTIL_PATH: $SHUTIL_PATH"
	exit 1
	;;
esac
