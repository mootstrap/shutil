#!/bin/sh

: ${SHUTIL_PATH:=shutil}
. "$SHUTIL_PATH/shutil2.sh"
shutil_require base
shutil_require event
shutil_require unit_test


# `advance_time` with no arguments is an error.
vcheck_error advance_time

assert [ "$(get_time)" -ne 0 ]

advance_time 0
assert [ "$(get_time)" -eq 0 ]

advance_time 5
assert [ "$(get_time)" -eq 5 ]

advance_time 7
assert [ "$(get_time)" -eq 12 ]

advance_time 0
assert [ "$(get_time)" -eq 12 ]

advance_time 1
assert [ "$(get_time)" -eq 13 ]

echo "Test complete: $prog"
