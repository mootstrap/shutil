#!/bin/sh

: ${SHUTIL_PATH:=shutil}
. "$SHUTIL_PATH/shutil2.sh"
shutil_require base
shutil_require event
shutil_require unit_test


set_time 0

assert [ "$(get_time)" -eq 0 ]
sleep_until_next_event

assert [ "$(get_time)" -eq 0 ]
assert is_event_ready a 30
assert is_event_ready b 50
assert is_event_ready c 60
sleep_until_next_event

assert [ "$(get_time)" -eq 30 ]
assert is_event_ready a 30
assert not is_event_ready b 50
assert not is_event_ready c 60
sleep_until_next_event

assert [ "$(get_time)" -eq 50 ]
assert not is_event_ready a 30
assert is_event_ready b 50
assert not is_event_ready c 60
sleep_until_next_event

assert [ "$(get_time)" -eq 60 ]
assert is_event_ready a 30
assert not is_event_ready b 50
assert is_event_ready c 60
sleep_until_next_event

assert [ "$(get_time)" -eq 90 ]
assert is_event_ready a 30
assert not is_event_ready b 50
assert not is_event_ready c 60
sleep_until_next_event

assert [ "$(get_time)" -eq 100 ]
assert not is_event_ready a 30
assert is_event_ready b 50
assert not is_event_ready c 60
sleep_until_next_event

set_time 280

assert [ "$(get_time)" -eq 280 ]
assert is_event_ready a 30
assert is_event_ready b 50
assert is_event_ready c 60
sleep_until_next_event

assert [ "$(get_time)" -eq 300 ]
assert is_event_ready a 30
assert is_event_ready b 50
assert is_event_ready c 60
sleep_until_next_event

assert [ "$(get_time)" -eq 330 ]


echo "Test complete: $prog"
