#!/bin/sh

: ${SHUTIL_PATH:=shutil}
. "$SHUTIL_PATH/shutil2.sh"
shutil_require base
shutil_require event
shutil_require unit_test


# `set_time` with no arguments is an error.
vcheck_error set_time

assert [ "$(get_time)" -ne 0 ]

set_time 0
assert [ "$(get_time)" -eq 0 ]

set_time 5
assert [ "$(get_time)" -eq 5 ]

set_time 7
assert [ "$(get_time)" -eq 7 ]


echo "Test complete: $prog"
