#!/bin/bash

: ${SHUTIL_PATH:=shutil}
. "$SHUTIL_PATH/shutil2.sh"
shutil_require base
shutil_require event
shutil_require unit_test


# Check usage messages.
vcheck_error backoff_run
vcheck_error backoff_run a


# The remainder of this test depends on bashisms (the event library does not,
# though).
if [ -z "$BASH" ]; then
    echo "Test complete: $prog"
    exit
fi

# backoff_run_test name periods... -- cmd args... -- wakeup_times....:
# Run backoff_run with `name`, `periods`, `cmd`, and `args`.  Verify that it
# wakes up at the times specified by `wakeup_times`.
backoff_run_test() {
    local name="$1" periods=() cmd=() wakeups=() w
    # Parse command line.
    [ -n "$name" ] || fatal_error "backof_run_test: A name is needed."
    shift

    while [ -n "$*" -a "$1" != "--" ]; do
        periods+=("$1")
        shift
    done

    [ "$1" = "--" ] && shift

    while [ -n "$*" -a "$1" != "--" ]; do
        cmd+=("$1")
        shift
    done

    [ "$1" = "--" ] && shift

    while [ -n "$*" -a "$1" != "--" ]; do
        wakeups+=("$1")
        shift
    done

    [ "$1" = "--" ] && shift
    assert [ -z "$*" ]

    vecho "periods=${periods[@]}"
    vecho "cmd=${cmd[@]}"
    vecho "wakeups=${wakeups[@]}"

    # Run backoff and check the time is correct.
    for w in "${wakeups[@]}"; do
        vrun backoff_run "$name" "${periods[@]}" -- "${cmd[@]}"
        vrun sleep_until_next_event
        vrun assert [ "$w" -eq "$(get_time)" ]
        vrun assert_eq w "$(get_time)"
    done
}


# Turn off verbose debugging output.
verbose=0

# Command returns success.  We should not back off.
set_time 0
backoff_run_test a 10 -- true -- $(seq 0 10 100)

set_time 0
backoff_run_test b 10 20 -- true -- $(seq 10 10 100)

# Command returns failure.  We will back off.
set_time 0
backoff_run_test c 10 -- false -- $(seq 10 10 100)

set_time 0
backoff_run_test d 10 100 -- false -- $(seq 10 10 100) $(seq 200 100 1000)


# A more complex example.
set_time 0
# Fail and back off through the periods.
backoff_run_test e 10 60 600 3600 -- false -- $(seq 10 10 59)
backoff_run_test e 10 60 600 3600 -- false -- $(seq 60 60 599)
backoff_run_test e 10 60 600 3600 -- false -- $(seq 600 600 3599)
backoff_run_test e 10 60 600 3600 -- false -- $(seq 3600 3600 86399)
# Succeed.
backoff_run_test e 10 60 600 3600 -- true -- $(seq 86400 10 87000)
# Fail again and back off through the periods again.
backoff_run_test e 10 60 600 3600 -- false -- $(seq 87010 10 87059)
backoff_run_test e 10 60 600 3600 -- false -- $(seq 87060 60 87599)


echo "Test complete: $prog"
