#!/bin/sh

: ${SHUTIL_PATH:=shutil}
. "$SHUTIL_PATH/shutil2.sh"
shutil_require base
shutil_require event
shutil_require unit_test


# Verify that the returned time increases.
t1="$(get_time)"
sleep 1
t2="$(get_time)"

assert [ "$t1" -lt "$t2" ]


# Test virtual time.  Empty SHUTIL_VIRTUAL_TIME does not enable virtual time.

SHUTIL_VIRTUAL_TIME=
assert [ "$t2" -le "$(get_time)" ]

SHUTIL_VIRTUAL_TIME=0
assert [ "$(get_time)" -eq 0 ]

SHUTIL_VIRTUAL_TIME=5
assert [ "$(get_time)" -eq 5 ]

SHUTIL_VIRTUAL_TIME=2
assert [ "$(get_time)" -eq 2 ]


echo "Test complete: $prog"
