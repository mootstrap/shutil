#!/bin/sh

: ${SHUTIL_PATH:=shutil}
. "$SHUTIL_PATH/shutil2.sh"
shutil_require base
shutil_require event
shutil_require unit_test


# Prior to calling reset_time_until_next_event, time_until_next_event always
# returns zero.
assert [ "$(time_until_next_event)" -eq 0 ]

set_time 0
assert [ "$(time_until_next_event)" -eq 0 ]

set_time 10
assert [ "$(time_until_next_event)" -eq 0 ]

assert is_event_ready a 300
assert [ "$(time_until_next_event)" -eq 0 ]

# reset_time_until_next_event resets shutil_next_event so that
# time_until_next_event will return an hour into the future.
reset_time_until_next_event
assert [ "$(time_until_next_event)" -eq 3600 ]

# Calculate an event after the first reset.
assert not is_event_ready a 300
assert [ "$(time_until_next_event)" -eq 290 ]

set_time 100
assert [ "$(time_until_next_event)" -eq 200 ]

# Check the return value of time_until_next_event.
dt="$(time_until_next_event)"
assert [ "$?" = 0 ]
assert_eq dt 200

# We have passed the nominal time, so we get a dt of 0 and a return of 1.
set_time 400
dt="$(time_until_next_event)"
assert [ "$?" = 1 ]
assert_eq dt 0

# Reset the scheduling.
reset_time_until_next_event
assert [ "$(time_until_next_event)" -eq 3600 ]

# Register events again.  "b" is later than "a" so time_until is unchanged.
# "c" moves the time_until up.
assert is_event_ready a 300
assert [ "$(time_until_next_event)" -eq 200 ]
assert is_event_ready b 350
assert [ "$(time_until_next_event)" -eq 200 ]
assert is_event_ready c 405
assert [ "$(time_until_next_event)" -eq 5 ]

# Advance time checking new time_untils.
set_time 405
reset_time_until_next_event
assert not is_event_ready a 300
assert not is_event_ready b 350
assert is_event_ready c 405
assert [ "$(time_until_next_event)" -eq 195 ]

set_time 600
reset_time_until_next_event
assert is_event_ready a 300
assert not is_event_ready b 350
assert not is_event_ready c 405
assert [ "$(time_until_next_event)" -eq 100 ]

# Test a period of zero.
set_time 610
reset_time_until_next_event
assert not is_event_ready a 300
assert not is_event_ready b 350
assert not is_event_ready c 405
assert [ "$(time_until_next_event)" -eq 90 ]

assert is_event_ready d 0
assert [ "$(time_until_next_event)" -eq 0 ]

# Period zero repeats as often as we ask.
assert is_event_ready d 0
assert [ "$(time_until_next_event)" -eq 0 ]

assert is_event_ready d 0
assert [ "$(time_until_next_event)" -eq 0 ]

# Advance time.  Other events still fire at appropriate times even though there
# is a zero period event firing as often as possible.
set_time 700
reset_time_until_next_event
assert not is_event_ready a 300
assert is_event_ready b 350
assert not is_event_ready c 405
assert is_event_ready d 0
assert [ "$(time_until_next_event)" -eq 0 ]


echo "Test complete: $prog"
