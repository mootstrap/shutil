#!/bin/sh

: ${SHUTIL_PATH:=shutil}
. "$SHUTIL_PATH/shutil2.sh"
shutil_require base
shutil_require event
shutil_require unit_test


# Check error checking.
assert [ "$(check_error is_event_ready 2>&1)" = \
    "is_event_ready requires an event name." ]

assert [ "$(check_error is_event_ready zzz 2>&1)" = \
    "is_event_ready requires a period." ]

# Set virtual time to 0.
set_time 0

# Run an event immediately, but then not again until 10 s.
assert is_event_ready a 10
assert not is_event_ready a 10

set_time 1
assert not is_event_ready a 10

set_time 9
assert not is_event_ready a 10

set_time 10
assert is_event_ready a 10

# Time jump.  We run once, and then wait for the next future deadline.
set_time 105
assert is_event_ready a 10
assert not is_event_ready a 10

set_time 109
assert not is_event_ready a 10

# Return to the normal phase despite the last event being 5 seconds ago.
set_time 110
assert is_event_ready a 10
assert not is_event_ready a 10

# Time jumps backwards (or equivalently leaps forward and wraps).  We have an
# immediate event.
set_time 25
assert is_event_ready a 10
assert not is_event_ready a 10

set_time 29
assert not is_event_ready a 10

# Back on the modulo 10 schedule with the new time.
set_time 30
assert is_event_ready a 10
assert not is_event_ready a 10


echo "Test complete: $prog"
