#!/bin/sh
# -*- sh -*-
# 

usage() {
    {
	cat <<EOF
usage: $prog [-dhnv] [-V n] cur_test cur_case [[new_test] new_case]
    Rename a new shutil test case.
args:
    cur_test
	The current test name.  Can be a number or other partial name as long
	as it matches uniquely one existing test name.
    cur_case
	The current test name.  Can be a number or part of a name.  Must
	uniquely match one existing case name inside the current test.
    new_test
	The new test name (if four arguments are provided).  Can be a number or
	other part of a name.  Must uniquely match an existing test name.  If
	only three arguments are provided, the new test name defaults to the
	current test name.
    new_case
	The new case name.  If it is not supplied, the current name is used,
	but the number will be reassigned to the first unused number.  If it is
	just a number, the rest of the current name will be kept.  If it is a
	name with no number, the first unused number is used.  If it is a full
	number and name, then it will be used as is.
options:
    -d
	Enable debugging (set -xv).
    -h
	Print this usage message.
    -v
	Verbose mode.
    -V n
	Set to verbosity level n.
EOF
    } >&2
    exit 1
}

######################################################################
# Find and source the shell utilities.

shutil_error() { echo "$@" >&2; exit 1; }
find_shutil2() {
    local d s
    for d in "$@"; do
        for s in . bin share/lib lib; do
            for SHUTIL_PATH in "$d/$s/shutil" "$d/$s/.shutil"; do
                [ -f "$SHUTIL_PATH/shutil2.sh" ] && return 0
            done
        done
    done
    shutil_error "Unable to find shutil2.sh"
}

script_dir="$(realpath "$(dirname "$0")")"
find_shutil2 "${SHUTIL_PATH:-$script_dir}" "${script_dir%/bin}" \
    "$HOME" "${SHUTIL_SYS:-/usr}"
. "$SHUTIL_PATH/shutil2.sh" || shutil_error "Error sourcing shutil2.sh"

# Require needed shutil modules.
shutil_require default


######################################################################
# Parse arguments.

#verbose=
while getopts :dhvV: OPT; do
    case $OPT in
	d|+d)
	    set -xv
	    ;;
	h|+h)
	    usage
	    ;;
	v|+v)
	    verbose=1
	    ;;
	V|+V)
	    verbose="$OPTARG"
	    ;;
	*)
	    usage
	    ;;
    esac
done
shift `expr $OPTIND - 1`

######################################################################

do_move() {
    local cmd=mv
    if [ -n "$1" ]; then
	cmd="echo ==> mv"
    fi
    for f in "${cur}"*; do
	ext="${f##*.}"
	$cmd -iv "$f" "${new}.${ext}"
    done
}



[ -z "$*" ] && usage

. "$script_dir/ut_util.sh"

verify_args "$prog cur_test cur_case [[new_test] new_case]" 2 4 "$@"
cur_test="$1"
cur_case="$2"
shift 2

new_test=
if [ "$#" -eq 2 ]; then
    new_test="$1"
    shift
fi
new_case="$1"

script_dir="$(make_absolute "$script_dir")"
vcd "$script_dir"

find_test cur_test "$cur_test" || \
    fatal_error "Failed to find cur test for \"$cur_test\"."
find_case cur "$cur_test" "$cur_case" || \
    fatal_error "Failed to find cur case for \"$cur_case\"."
cur="${cur%.sh}"
cur_case="$(basename "$cur")"

cur_cname="$cur_case"
cur_cname="${cur_cname#case_}"
cur_cname="${cur_cname#[0-9]}"
cur_cname="${cur_cname#[0-9]}"
cur_cname="${cur_cname#[0-9]}"
cur_cname="${cur_cname#[0-9]}"
cur_cname="${cur_cname#_}"

[ -z "$new_test" ] && new_test="$cur_test"
find_test new_test "$new_test" || \
    fatal_error "Failed to find new test for \"$new_test\"."

# Find the first unused number in the new test, in case we need it.
new_num="$(find_free_number "$new_test/case_%02d" "%d")"

if [ -z "$new_case" ]; then
    # No new case was specified, so we are going to renumber.
    if [ -z "$cur_cname" ]; then
	# The old name had nothing but a number, so we just move to the new
	# number.
	new_case="$(printf 'case_%02d' "${new_num}")"
    else
	# The old name has a name, so we keep that with the new number.
	new_case="$(printf 'case_%02d_%s' "${new_num}" "$cur_cname")"
    fi
else
    # A new case was supplied.  Split it to find if we need to supply a number
    # or reuse the old name.
    new_case="${new_case#case_}"

    # If we were given a number, use it instead of the new_num we have now.
    split_num="$(echo "$new_case" | sed -n 's/^\([0-9][0-9]*\).*/\1/p')"
    [ -n "$split_num" ] && new_num="${split_num}"

    new_cname="${new_case}"
    new_cname="${new_cname#[0-9]}"
    new_cname="${new_cname#[0-9]}"
    new_cname="${new_cname#[0-9]}"
    new_cname="${new_cname#[0-9]}"
    new_cname="${new_cname#_}"

    assert_not_null new_num

    # Reuse the old case name if we were not given a new one.
    : "${new_cname:=$cur_cname}"

    if [ -n "$new_cname" ]; then
	# We have a number and a name.
	new_case="$(printf 'case_%02d_%s' "${new_num}" "$new_cname")"
    else
	# We only have a number.
	new_case="$(printf 'case_%02d' "${new_num}")"
    fi
fi
new="$new_test/$new_case"

echo "cur: $cur ($cur_test / $cur_case)"
echo "new: $new ($new_test / $new_case)"

assert_not_null cur new
assert directory_exists "$cur_test"
assert file_exists "$cur.sh"
assert directory_exists "$new_test"
assert not exists "$new"*

do_move 1
yn "Continue?" y || exit
do_move

#EOF
