#!/bin/sh
# -*- sh -*-
# 

usage() {
    {
	cat <<EOF
usage: $prog [-dhv] [-V n] test_name [case_name]
    Add a new shutil test and test case.
options:
    -d
	Enable debugging (set -xv).
    -h
	Print this usage message.
    -v
	Verbose mode.
    -V n
	Set to verbosity level n.
EOF
    } >&2
    exit 1
}

######################################################################
# Find and source the shell utilities.

shutil_error() { echo "$@" >&2; exit 1; }
find_shutil2() {
    local d s
    for d in "$@"; do
        for s in . bin share/lib lib; do
            for SHUTIL_PATH in "$d/$s/shutil" "$d/$s/.shutil"; do
                [ -f "$SHUTIL_PATH/shutil2.sh" ] && return 0
            done
        done
    done
    shutil_error "Unable to find shutil2.sh"
}

script_dir="$(realpath "$(dirname "$0")")"
find_shutil2 "${SHUTIL_PATH:-$script_dir}" "${script_dir%/bin}" \
    "$HOME" "${SHUTIL_SYS:-/usr}"
. "$SHUTIL_PATH/shutil2.sh" || shutil_error "Error sourcing shutil2.sh"

# Require needed shutil modules.
shutil_require default


######################################################################
# Parse arguments.

#verbose=
while getopts :dhvV: OPT; do
    case $OPT in
	d|+d)
	    set -xv
	    ;;
	h|+h)
	    usage
	    ;;
	v|+v)
	    verbose=1
	    ;;
	V|+V)
	    verbose="$OPTARG"
	    ;;
	*)
	    usage
	    ;;
    esac
done
shift `expr $OPTIND - 1`

######################################################################

# find_free_number check_pat name_pat
# Find a free number to use to (for example) number a case name.  The first
# agument is a printf pattern of name prefixes to check while the second is a
# printf pattern of name prefixes to use once a free number has been found.
#
# Example:
#     find_free_number "case_%02d" "case_%02d_name_of_case"
#
# This will search (starting at case_00*, the case_01* and so on) until if
# finds a number which is unused.  If the first unused number was 5, then it
# would return "case_05_name_of_case".
find_free_number() {
    local check_pat="$1" name_pat="$2" n=0 check_name name
    while true; do
	check_name="$(printf "$check_pat" "$n")"
	if exists "$check_name"*; then
	    n="$((n + 1))"
	    continue
	else
	    printf "$name_pat\n" "$n"
	    return
	fi
    done

    fatal_error "Unreachable."
}

fix_names() {
    local case_path

    # First check if we can find an existing test from the name the user gave
    # us.
    if not find_test test_name "$test_name"; then
	# Not found, so ensure there is a "test_" at the front of the test
	# name the user supplied.
	test_name="test_${test_name#test_}"
    fi

    # See if we can find an existing test case.
    if find_case case_path "$test_name" "$case_name"; then
	# Found it.  Fix up the variables and return.
	assert_eq test_name "$(dirname "$case_path")"
	case_name="$(basename "$case_path" .sh)"
	return
    fi

    # The test and/or case do not exist.  Find a proper case name to use with
    # the test name we fixed above.

    # Find the first free number in case we need it.
    free_num="$(find_free_number "${test_name}/case_%02d" "%d")"

    if [ -z "$case_name" ]; then
	# The user didn't specify a name, so we choose a number-only name.
	case_name="$(printf "case_%02d" "$free_num")"
    else
	# The user specified a name.  Strip "case_" and ".sh" if present.
	case_name="${case_name#case_}"
	case_name="${case_name%.sh}"
	if [ -f "$test_name/case_$case_name.sh" ]; then
	    # The user specified name exists.  Use it as it.
	    :
	else
	    # Check if they included a number.
	    cname="$case_name"
	    cname="${cname#[0-9]}"
	    cname="${cname#[0-9]}"
	    cname="${cname#[0-9]}"
	    cname="${cname#[0-9]}"
	    cname="${cname#_}"

	    if [ "$cname" = "$case_name" ]; then
		# No number was supplied.
		if [ -n "$cname" ]; then
		    # They supplied a name, but no number.  Use their name and
		    # the free number.
		    case_name="$(printf "case_%02d_%s" "$free_num" "$cname")"
		else
		    # They didn't specify a name or number.
		    case_name="$(printf "case_%02d" "$free_num")"
		fi
	    else
		# A number was supplied.  We just use their full name as is.
		:
	    fi
	fi
    fi

    # Normalize the name.
    case_name="case_${case_name#case_}"
    case_name="${case_name%.sh}"

    vecho "Creating/editing $test_name/$case_name.sh"
}

add_test() {
    [ -d "$test_name" ] && return
    [ -e "$test_name" ] && \
	fatal_error "$test_name exists and is not a directory."

    yn "Create test \"$test_name\"?" y || \
	fatal_error "Aborting..."

    mkdir "$test_name"
    prepare="$test_name/prepare"

    cat >"$prepare" <<'EOF'
#!/bin/sh

# Create a shutil link.
ln -s "$SHUTIL_PATH" "$2/shutil"
EOF

    chmod 755 "$prepare"
    ${VISUAL:-${EDITOR:-vi}} "$prepare"
}

add_case() {
    local c="$test_name/$case_name.sh"

    [ -e "$c" -a ! -f "$c" ] && \
	fatal_error "$c exists but is not a file."

    if [ ! -f "$c" ]; then
	yn "Create test case \"$c\"?" y || \
	    fatal_error "Aborting..."

	cat >"$c" <<'EOF'
#!/bin/sh

: ${SHUTIL_PATH:=shutil}
. "$SHUTIL_PATH/shutil2.sh"
shutil_require base
shutil_require unit_test

echo "Test complete: $prog"
EOF

	chmod 755 "$c"
    fi

    ${VISUAL:-${EDITOR:-vi}} "$c"
    vrun ./run_tests -u "$test_name/$case_name.sh"
}

[ -n "$1" ] || usage

. "$script_dir/ut_util.sh"

verify_args "$prog test_name case_name" 1 2 "$@"
test_name="$1"
shift
case_name="$1"

script_dir="$(make_absolute "$script_dir")"
vcd "$script_dir"

fix_names
add_test "$test_name"
add_case "$case_name"

#EOF
