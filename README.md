shutil
======

The shutil library is a library of shell utility functions.  "Version 0"
(pre-2000) was a set of useful functions copied from script to script.  By
2000, the set had grown.  I broke the common functions out into their own file,
shutil.sh, creating shutil version 1.  This was also when it started being
tracked under version control along with my dot files and bin directory.  Since
every new script would now just source shutil.sh and generally include a few
other things (like a usage message and argument parsing), I also developed a
template for new scripts (and a script called newcmd to create new scripts
according to that template).

For shutil version 2, the first version released publicly, I divided the one
monolithic file into modules, revised and rewrote much of it for style,
consistency, and safety, and added documentation and unit tests (and fixed a
few bugs the tests revealed).


## Typical Usage

```console
$ cd "$HOME/bin"
$ git clone https://gitlab.com/mootstrap/shutil.git shutil
$ ln -s shutil/bin/newcmd .
```

This will checkout the shutil git repo into `$HOME/bin` and link the newcmd
script into that directory as well.  (If you prefer, you could just add
`$HOME/bin/shutil/bin` to PATH.)  

Now, as long as `$HOME/bin` is in your PATH, you can create new commands like
so:

```console
$ newcmd my_new_cmd
```

Running newcmd brings up an editor on the supplied script name, creating it
from the template and making it executable if it did not already exist.
Absolute paths and relative paths with a directory part (that is any path with
a slash in it) are interpreted like normal.  Bare names (names with no slashes
in them) are interpreted relative to `$HOME/bin`.  As such, the command above
creates and edits `$HOME/bin/my_new_cmd`.

The template includes a usage message, code to find and source shutil2.sh, a
call to `shutil_require default`, and code to parse arguments.  In the body of
the script, all functions defined in the default module (primarily see
[mod/base.sh](mod/base.sh) for documentation) are available for use.  If
additional modules are required, then next to the `shutil_require default` call
you can use `shutil_require other_mod` as needed.
